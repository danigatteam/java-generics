# README #

Example from https://app.pluralsight.com/library/courses/java-generics/table-of-contents

## Examples ##

See application.java:

- Basic generic array (com.quintopino.basics)
- Better to use generic collections (com.quintopino.collections)
- Generics and Interfaces (com.quintopino.genericinterfaces)
    + Implement a generic type (Comparator), sort a list
    + b
    + c
