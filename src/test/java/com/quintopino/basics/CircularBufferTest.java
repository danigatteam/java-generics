package com.quintopino.basics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CircularBufferTest {
    private int size = 2;
    private CircularBuffer buffer = new CircularBuffer(size);

    @Test
    public void shouldOfferPollableElement() {
	assertTrue(buffer.offer(1));
	assertEquals(1, buffer.poll());
	assertNull(buffer.poll());
    }

    @Test
    public void shouldNotOfferWhenBufferIsFull() {
	buffer.offer(1);
	buffer.offer(2);
	assertFalse(buffer.offer(3));
    }

    @Test
    public void shouldNotPollWhenBufferIsEmpty() {
	assertNull(buffer.poll());
    }

    @Test
    public void shouldRecycleBuffer() {
	buffer.offer(1);
	buffer.offer(2);
	assertEquals(1, buffer.poll());
	assertEquals(2, buffer.poll());
	assertTrue(buffer.offer(3));
	assertTrue(buffer.offer(4));
    }

}
