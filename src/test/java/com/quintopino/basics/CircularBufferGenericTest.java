package com.quintopino.basics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CircularBufferGenericTest {
    private int size = 2;
    private CircularBufferGeneric<String> buffer = new CircularBufferGeneric<>(size);

    @Test
    public void shouldOfferPollableElement() {
	assertTrue(buffer.offer("a"));
	assertEquals("a", buffer.poll());
	assertNull(buffer.poll());
    }

    @Test
    public void shouldNotOfferWhenBufferIsFull() {
	buffer.offer("a");
	buffer.offer("b");
	assertFalse(buffer.offer("c"));
    }

    @Test
    public void shouldNotPollWhenBufferIsEmpty() {
	assertNull(buffer.poll());
    }

    @Test
    public void shouldRecycleBuffer() {
	buffer.offer("a");
	buffer.offer("b");
	assertEquals("a", buffer.poll());
	assertEquals("b", buffer.poll());
	assertTrue(buffer.offer("c"));
	assertTrue(buffer.offer("d"));
    }

}
