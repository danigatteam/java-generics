package com.quintopino;

import com.quintopino.basics.CircularBuffer;
import com.quintopino.basics.CircularBufferGeneric;

public class Application {

    public static void main(String[] params) {
	// basic generic array
	typeUnsafe();
	genericType();
    }

    private static void genericType() {
	CircularBufferGeneric<String> buffer = new CircularBufferGeneric<>(5);

	buffer.offer("a");
	buffer.offer("bc");
	buffer.offer("d");

	System.out.println(concatenateGeneric(buffer));
    }

    private static void typeUnsafe() {
	CircularBuffer buffer = new CircularBuffer(5);
	buffer.offer("a");
	buffer.offer("bc");
	buffer.offer("d");

	System.out.println(concatenate(buffer));
    }

    private static String concatenate(CircularBuffer buffer) {

	StringBuilder out = new StringBuilder();

	String item;
	while ((item = (String) buffer.poll()) != null) {
	    out.append(item);
	}
	return out.toString();
    }

    private static String concatenateGeneric(CircularBufferGeneric buffer) {

	StringBuilder out = new StringBuilder();

	String item;
	while ((item = (String) buffer.poll()) != null) {
	    out.append(item);
	}
	return out.toString();
    }

}
