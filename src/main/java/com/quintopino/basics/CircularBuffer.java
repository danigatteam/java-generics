package com.quintopino.basics;

public class CircularBuffer {

    private final Object[] buffer;
    private int readCursor = 0;
    private int writeCursor = 0;

    public CircularBuffer(int size) {
	buffer = new Object[size];
    }

    public Boolean offer(Object item) {
	if (buffer[writeCursor] != null) {
	    return false;
	}
	buffer[writeCursor] = item;
	writeCursor = next(writeCursor);
	return true;
    }

    public Object poll() {
	Object value = buffer[readCursor];
	if (value != null) {
	    buffer[readCursor] = null;
	    readCursor = next(readCursor);
	}
	return value;
    }

    private int next(int index) {
	return (index + 1) % buffer.length;
    }

}
