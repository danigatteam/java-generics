package com.quintopino.basics;

public class CircularBufferGeneric<T> {

    private final T[] buffer;
    private int readCursor = 0;
    private int writeCursor = 0;

    public CircularBufferGeneric(int size) {
	buffer = (T[]) new Object[size];
    }

    public Boolean offer(T item) {
	if (buffer[writeCursor] != null) {
	    return false;
	}
	buffer[writeCursor] = item;
	writeCursor = next(writeCursor);
	return true;
    }

    public Object poll() {
	T value = buffer[readCursor];
	if(value != null) {
	    buffer[readCursor] = null;
	    readCursor = next(readCursor);
	}
	return value;
    }

    private int next(int index) {
	return (index + 1) % buffer.length;
    }

}
