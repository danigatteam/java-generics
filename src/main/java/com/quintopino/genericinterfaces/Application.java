package com.quintopino.genericinterfaces;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.quintopino.models.Person;

public class Application {

    static Person esther = new Person("Esther", 12);
    static Person miriam = new Person("Miriam", 22);
    static Person pepito = new Person("Pepito", 31);
    
    public static void main(String[] args) {
	// TODO Auto-generated method stub
	
	List<Person> people = new ArrayList<>();
	people.add(pepito);
	people.add(esther);
	people.add(miriam);
	
	System.out.println("Before sort: " + people);

	Collections.sort(people, new PersonComparator());
	
	System.out.println("After sort: " + people);
    }

}
