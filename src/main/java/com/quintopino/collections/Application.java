package com.quintopino.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.quintopino.models.Person;

import java.util.Set;

public class Application {

    static Person esther = new Person("Esther", 12);
    static Person miriam = new Person("Miriam", 22);
    static Person pepito = new Person("Pepito", 31);

    public static void main(String[] args) {

	System.out.println("---- List");
	doLists();

	System.out.println("---- Set");
	doSets();

	System.out.println("---- Map");
	doMaps();
    }

    private static void doLists() {

	// not necessary to set the size
	List<Person> people = new ArrayList<>();

	// automatically resized
	people.add(pepito);
	people.add(esther);
	people.add(miriam);

	// integrated toString
	System.out.println(people);

	// are sorted, can access one element
	System.out.println(people.get(2));

	// iterate
	System.out.println("-- Loop with for loop");
	for (int i = 0; i < people.size(); i++) {
	    System.out.println(people.get(i));
	}

	System.out.println("-- Loop with iterator");
	Iterator<Person> iterator = people.iterator();
	while (iterator.hasNext()) {
	    System.out.println(iterator.next());
	}

	System.out.println("-- Loop with foreach");
	for (Person person : people) {
	    System.out.println(person);
	}
    }

    private static void doMaps() {
	Map<String, Person> people = new HashMap<>();

	// associate value with a key
	people.put("pepito's key", pepito);
	System.out.println(people.get("pepito's key"));

	// keys are unique
	people.put("another key", esther);
	people.put("another key", miriam);
	System.out.println(people.get("another key"));

	System.out.println("-- Loop for keys");
	for (String key : people.keySet()) {
	    System.out.println(key);
	}
	System.out.println("-- Loop for values");
	for (Person value : people.values()) {
	    System.out.println(value);
	}
	System.out.println("-- Loop for entries");
	for (Entry<String, Person> entry : people.entrySet()) {
	    System.out.println(entry);
	    System.out.println(entry.getKey() + " --> " + entry.getValue());
	}
    }

    private static void doSets() {
	Set<Person> people = new HashSet<>();

	// Sets don't allow duplicates
	people.add(pepito);
	people.add(esther);
	people.add(pepito);

	System.out.println(people);

	System.out.println("-- Loop with foreach");
	for (Person person : people) {
	    System.out.println(person);
	}
    }

}
